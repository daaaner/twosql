![Twitter Logo](https://upload.wikimedia.org/wikipedia/de/thumb/9/9f/Twitter_bird_logo_2012.svg/150px-Twitter_bird_logo_2012.svg.png)

# Twitter to SQL

This projects takes the approach of crawling specific twitter data and pump them into a MySQL-DB. The Idea ist to provide a microservice for perople who rely on structured dbs for their purposes (Analytics, Bi, ...)

## Structure

1. Provide an interface where user authenticate via oAuth
2. Setup internal db for user-mgm
3. Setup db configurations 
3. Crawl data
4. Set time intervalls


## Setup

First - Checkout

Install all gems from Gemfile:

~~~
bundle install
~~~

Then migrate/create db

~~~
rake db:migrate
~~~

If all worked fine, then you should be able to startup the server

~~~
rails s
~~~
