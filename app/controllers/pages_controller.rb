class PagesController < ApplicationController
  def index

  	# if no session exists nothing gets send to the view
  	if current_user 
  		@connection = current_user.connections.new
  		@connections = current_user.connections.where("id is NOT NULL")
  	end

  end
end