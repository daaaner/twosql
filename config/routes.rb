Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'pages#index'

  resources :users do
  	resources :connections
	end
  
  # Now let’s return to the Callback URL. This is the URL where a user will be redirected to inside 
  # the app after successful authentication and approved authorization (the request will also contain 
  # user’s data and token). All OmniAuth strategies expect the callback URL to equal 
  # to “/auth/:provider/callback”. :provider takes the name of the strategy (“twitter”, “facebook”, 
  # “linkedin”, etc.) as listed in the initializer.

  get '/auth/:provider/callback', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

end
