class ConnectionsController < ApplicationController

def create
	# unless current_user -> send to index
	user = User.find(params[:user_id])
	@connection = user.connections.new(connction_params)
	@connection.provider = current_user.provider
    if @connection.save
      puts "Success"
      flash[:success] = "Connection successfully added"
      redirect_to root_path
    else
      render 'new'
    end

end



private


def connction_params
    params.require(:connection).permit(:provider, :url, :port, :db_user, :ssl, :ssl_trust, :password,
                                   :password_confirmation, :ca_cert, :user_cert, :user_key)
end

end