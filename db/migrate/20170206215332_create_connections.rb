class CreateConnections < ActiveRecord::Migration[5.0]
  def change
    create_table :connections do |t|
      t.belongs_to :user, index: true
      t.string :provider, null: false 
      t.string :url, null: false 
      t.string :port, null: false, default: "3306"
      t.string :db_user, null: false 
      t.string :password_digest, null: false 
      t.boolean :ssl, default: false
      t.boolean :ssl_trust, default: false
      t.string :ca_cert
      t.string :user_cert
      t.string :user_key                      # serialize and bcrypt please
      t.timestamps
    end
  end
end
